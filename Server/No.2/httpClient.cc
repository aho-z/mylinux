#include "httpClient.hpp"

#include <memory>
void Usage(const std::string msg)
{
    std::cout << "\nUsage:\n\t" << msg << " server_ip+server_port\n\n";
}

int main(int argc,char* argv[])
{

    if(argc!=2)
    {
        Usage(argv[0]);
        exit(USAGE_ERR);
    }

    std::string domainName=argv[1];
    std::unique_ptr<httpClient> hcli(new httpClient(domainName));
    hcli->init();
    hcli->start();

    return 0;
}