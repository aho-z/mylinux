#include "Client.hpp"

#include <memory>
using namespace std;
using namespace onlineClient;

void Usage(const std::string msg)
{
    std::cout << "\nUsage:\n\t" << msg << " server_ip+server_port\n\n";
}

int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        Usage(argv[0]);
        exit(USAGE_ERR);
    }

    std::string ip=argv[1];
    uint16_t port=atoi(argv[2]);

    unique_ptr<Client> cli(new Client(ip,port));
    cli->init();
    cli->run();
    return 0;
}