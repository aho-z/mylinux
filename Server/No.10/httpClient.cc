#include "httpClient.hpp"

int main() {
    std::string request = "GET / HTTP/1.1\r\nHost: " + TARGET_HOST + "\r\nConnection: close\r\n\r\n";
    httpRequest(request);

    return 0;
}