#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <netdb.h>
#include <fcntl.h>


#include "errno.hpp"
#include "Logmessage.hpp"
class Socket
{
    const static uint16_t gbacklog=5;
public:
    static int CreateSock(int type=SOCK_STREAM,int protocol=0)
    {
        
        int socketfd=socket(AF_INET,type,protocol);
        if(socketfd<0)
        {
            logMessage(FATAL,"create socket err");
            exit(SOCKET_ERR);
        }

        
        logMessage(NORMAL,"create socket success...");
        return socketfd;

    }
    //设置地址复用
    static void Setsockopt(int listen_fd)
    {
        int opt=1;
        setsockopt(listen_fd,SOL_SOCKET,SO_REUSEADDR | SO_REUSEPORT,&opt,sizeof(opt));
        logMessage(NORMAL,"setsockopt success...");
    }

    static void Bind(int sock,int port)
    {
        struct sockaddr_in local;
        memset(&local,0,sizeof (local));
        local.sin_family=AF_INET;
        local.sin_port=htons(port);
        local.sin_addr.s_addr=INADDR_ANY;

        if(bind(sock,(struct sockaddr*)&local,sizeof(local)))
        {
            logMessage(FATAL,"bind err...");

            exit(BIND_ERR);
        }
        logMessage(NORMAL,"bind success...");

    }

    static void Listen(int sock)
    {
        if(listen(sock,gbacklog)<0)
        {
            logMessage(FATAL,"listen err...");
            exit(LISTEN_ERR);
        }
        logMessage(NORMAL,"listen socket success...");

    }

    static int Accept(int sock,std::string* clientip,uint16_t* clientport)
    {
        struct sockaddr_in peer;
        socklen_t len=sizeof(peer);
        int sockfd=accept(sock,(struct sockaddr*)&peer,&len);
        if(sock<0)
        {
            logMessage(WARNING,"accept err...");
        }
        else
        {
            *clientip=inet_ntoa(peer.sin_addr);
            *clientport=ntohs(peer.sin_port);
        }

        return sockfd;
    }

    static int Connect(int sock,std::string& clientip,uint16_t clientport)
    {
        struct sockaddr_in server;
        memset(&server,0,sizeof (server));
        server.sin_family=AF_INET;
        server.sin_port=htons(clientport);
        server.sin_addr.s_addr=inet_addr(clientip.c_str());

        return connect(sock,(const sockaddr*)&server,sizeof(server));
        
    }


    static char* getIpByDomainName(const std::string& domainName)
    {
        // 根据域名解析ip地址
        struct hostent *hs = gethostbyname(domainName.c_str());
        if (hs == NULL) {
            logMessage(WARNING," analysis domainName err");
            exit(ANALYSIS_ERR);
        }

        struct in_addr in;
        in.s_addr=*((uint32_t*)hs->h_addr_list[0]) ;
        char* addr = inet_ntoa(in);

        return addr;   
    }

    // 将文件描述符设置为非阻塞
    // 使用F_GETFL将当前的文件描述符的属性取出来(这是一个位图).
    // 然后再使用F_SETFL将文件描述符设置回去. 
    // 设置回去的同时, 加上一个O_NONBLOCK参数.
    static void setNonBlock(int fd)
    {
        //获得文件状态标记
        int f1=fcntl(fd,F_GETFL);
        if(f1<0)
        {
            std::cerr<<"fcntl: "<<strerror(errno)<<std::endl;
            return;
        }
        //设置文件描述符标记
        fcntl(fd,F_SETFL,f1|O_NONBLOCK);
    }



};


