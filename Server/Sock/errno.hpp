    #pragma once
    
    enum
    {
        USAGE_ERR = 1,
        SOCKET_ERR,
        BIND_ERR,
        LISTEN_ERR,
        ANALYSIS_ERR,
        SEND_ERR,
        CREATE_FILE_ERR,
        RECV_ERR
    };