#include "selectServer.hpp"

#include <memory>

using namespace std;
using namespace selectIO;

std::string transaction(const std::string &request)
{
    return "server recv"+request;
}


int main(int argc, char *argv[])
{

    unique_ptr<selectServer> svr(new selectServer(transaction));

    svr->init();

    svr->run();

    return 0;
}