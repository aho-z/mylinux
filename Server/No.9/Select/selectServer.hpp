#pragma once
#include <iostream>
#include <functional>
#include <sys/select.h>

#include "../../Sock/Socket.hpp"
#include "../../Sock/Logmessage.hpp"
#include "../../Sock/errno.hpp"

namespace selectIO
{
    static const uint16_t defaultport=8083;
    static const int fdSize=sizeof(fd_set)*8;//监控的文件描述符数组大小,fd_set是128字节，总共能监听1024个任务
    static const int defaultfd=-1;
    using func_t=std::function<std::string(const std::string&)>;

    class selectServer
    {
        uint16_t  port_;
        int listen_fd_;
        func_t callback_;
        int* fdarray;
    public:
        selectServer(func_t callback,const uint16_t port=defaultport)
            :callback_(callback),listen_fd_(-1),port_(port)
        {}    
        void init()
        {
            listen_fd_=Socket::CreateSock();
            Socket::Setsockopt(listen_fd_);
            Socket::Bind(listen_fd_,port_);
            Socket::Listen(listen_fd_);

            //初始化文件描述符数组
            fdarray=new int [fdSize];
            for(int i=0;i<fdSize;++i) fdarray[i]=defaultfd;
            fdarray[0]=listen_fd_;
        }

        void run()
        {
            for(;;)
            {
                fd_set rfds;//读文件描述符集
                FD_ZERO(&rfds);
                int maxFd=fdarray[0];//初始化select第一个参数
                //遍历查找最大的文件描述符和有效的文件描述符
                for(int i=0;i<fdSize;++i)
                {   
                    if(fdarray[i]==defaultfd) continue;
                    FD_SET(fdarray[i],&rfds);//将有效的文件描述符设置进读文件集
                    if(maxFd<fdarray[i]) maxFd=fdarray[i];
                }

                logMessage(NORMAL,"maxFd is %d",maxFd);

                int n=select(maxFd+1,&rfds,nullptr,nullptr,nullptr);//阻塞式等待
                switch (n)
                {
                case 0:
                    logMessage(NORMAL,"timeout...");
                    break;
                case -1:
                    logMessage(WARNING, "select error, code: %d, err string: %s", errno, strerror(errno));
                    break;

                default:
                    // 说明有事件就绪了
                    logMessage(NORMAL, "have event ready!");
                    HandlerReadEvent(rfds);
                    break;
                }

            }

        }

        void HandlerReadEvent(fd_set& rfds)
        {
            for(int i=0;i<fdSize;++i)
            {
                // 过滤非法的fd
                if (fdarray[i] == defaultfd)
                    continue;

                // 正常的
                if (FD_ISSET(fdarray[i], &rfds) && fdarray[i] == listen_fd_) // listensock负责接收
                {
                    Accepter(listen_fd_);
                }
                else if (FD_ISSET(fdarray[i], &rfds)) // 其余的就绪,sock负责读取
                {
                    Recver(fdarray[i], i);
                }
                else
                {
                    logMessage(WARNING,"null");
                }
            }
        }

        void Accepter(int lsock)
        {
            logMessage(DEBUG,"listen sock accept begin");
            std::string clientIp;
            uint16_t clientPort;
            //获取连接
            int sockfd=Socket::Accept(lsock,&clientIp,&clientPort);
            if(sockfd<0) return;
            logMessage(NORMAL,"accept success [%s/%d]",clientIp,clientPort);
            //将获取到的连接添加到fdarray数组
            int idx=0;
            for(;idx<fdSize;++idx)//寻找空闲位置
            {
                if(fdarray[idx]!=defaultfd) continue;
                else break;
            }
            // 判断是否越界
            if(idx==fdSize)
            {
                logMessage(WARNING,"Server fdarrar isFull,wait...");
                close(sockfd);
            }
            else fdarray[idx]=sockfd;//将fd添加到数组中

            // fdArrayPrint();

            logMessage(DEBUG,"listen sock accept end");
        }

        void Recver(int sock,int pos)
        {
            logMessage(DEBUG," recv  begin");
            char buffer[1024];
            ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0); // 不会被阻塞
            if (s > 0)
            {
                buffer[s] = 0;
                logMessage(NORMAL, "client# %s", buffer);
            }
            else if (s == 0)//客户端退出
            {
                close(sock);//close之后需要在维护的数组中去掉
                fdarray[pos] = defaultfd;
                logMessage(NORMAL, "client quit");
                return;
            }
            else
            {
                close(sock);
                fdarray[pos] = defaultfd;
                logMessage(ERROR, "client quit: %s", strerror(errno));
                return;
            }
            // 2. 处理request
            std::string response = callback_(buffer);

            // 3. 返回response
            write(sock, response.c_str(), response.size()); 


            logMessage(DEBUG," recv  end");
        }


        ~selectServer()
        {
            if(listen_fd_>=0) close(listen_fd_);
            if(fdarray) 
            {
                for(int i = 0; i < fdSize; ++i)
            {
                if(fdarray[i] != defaultfd)
                {
                    close(fdarray[i]);
                }
            }
                delete[] fdarray;
            }            

        }


        void fdArrayPrint()
        {
            std::cout<<"fdarray"<<std::endl;
            for(int i=0;i<fdSize;++i)
            {
                if(fdarray[i]!=defaultfd)
                    std::cout<<fdarray[i]<<" ";
            }
            printf("\n");
        }
    };
}
