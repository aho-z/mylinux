#include "Server.hpp"
#include <memory>
using namespace httpsServer;

void Usage(const string msg)
{
     cout << "\nUsage:\n\t" << msg << " local_port\n\n";
}

int main(int argc,char* argv[])
{
    if(argc!=2)
    {
        Usage(argv[0]);
        exit(USAGE_ERR);
    }
    
    uint16_t port=atoi(argv[1]);
    unique_ptr<Server> svr(new Server(port));
    svr->init();
    svr->run();
    return 0;
}