#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include "SSL.hpp"

#include "../Sock/Socket.hpp"
#include "../Sock/errno.hpp"
#include "../Sock/Logmessage.hpp"

namespace httpsClient
{
    class Client
    {
        std::string ip_;
        uint16_t  port_;
        int sockfd_;
    public:
        Client(const std::string& ip,const uint16_t port)
            :ip_(ip),port_(port),sockfd_(-1)
        {}

        void init()
        {
            sockfd_=Socket::CreateSock();
        }

        void run()
        {
            if(Socket::Connect(sockfd_,ip_,port_)!=0)
            {
                logMessage(WARNING,"connect err...");
            }
            serverIO();
        }
        ~Client()
        {
            if(sockfd_>=0)
                close(sockfd_);
        }
    private:
        void serverIO()
        {
            std::string msg;
            SSL* ssl=sync_initialize_ssl(SSL_MODE_CLIENT,sockfd_);
            while (true)
            {
                std::cout << "Enter# ";
                getline(std::cin, msg);
                //修改为加密写，解密读
                SSL_write(ssl, msg.c_str(), msg.size());

                char buffer [1024];
                int n=SSL_read(ssl,buffer,sizeof(buffer)-1);
                if (n>0)
                {
                    buffer[n]=0;
                    std::cout<<"Server处理后为# "<<buffer<<std::endl;
                }
                else
                {
                    break;
                }
            }
            //关闭和释放资源
            SSL_shutdown(ssl);
            SSL_free(ssl);
        }      
    };
}