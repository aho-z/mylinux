#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include "SSL.hpp"

#include "../Sock/Socket.hpp"
#include "../Sock/errno.hpp"
#include "../Sock/Logmessage.hpp"

namespace httpsServer
{
    using namespace std;
    static const uint16_t gport=8080;
    
    class Server
    {
        uint16_t  port_;
        int listen_fd_;
    public:
        Server(const uint16_t port=gport)
            :listen_fd_(-1),port_(port)
        {}    
        void init()
        {
           
            listen_fd_=Socket::CreateSock();
            Socket::Setsockopt(listen_fd_);
            Socket::Bind(listen_fd_,port_);
            Socket::Listen(listen_fd_);
        }

        void run()
        {
            for(;;)
            {
                std::string clientip;
                uint16_t clientport=0;
                int sock=Socket::Accept(listen_fd_,&clientip,&clientport);
                if(sock<0)
                {
                    logMessage(WARNING,"accept err...");
                    continue;
                }
                logMessage(NORMAL,"accept sock [%d] success...",sock);

                serverIO(sock);
                close(sock);
                
            }

        }
        ~Server(){}

    private:
        void serverIO(int sock)
        {
            SSL* ssl=sync_initialize_ssl(SSL_MODE_SERVER,sock);
            char  buffer[1024];
            while (true)
            {   ////修改为加密写，解密读
                int n=SSL_read(ssl,buffer,sizeof(buffer)-1);
                if(n>0)
                {
                    buffer[n]=0;
                    string outbuffer=buffer;
                    cout<<"recv from client"<<outbuffer<<endl;
                    SSL_write(ssl,outbuffer.c_str(),outbuffer.size());
                }
                else if(n==0)
                {
                    logMessage(NORMAL,"client quit and I also quit");
                    break;
                }

            }
            //关闭连接并释放资源
            SSL_shutdown(ssl);
            SSL_free(ssl);
            
        }
        
    };
}