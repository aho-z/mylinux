#include "icmpClient.hpp"
#include <memory>
using namespace std;
using namespace Client;

static void Usage(string proc)
{
    cout << "\nUsage:\n\t" <<proc << " server_name \n\n";
}


int main(int argc,char* argv[])
{
    if(argc!=2)
    {
        Usage(argv[0]);
        exit(USAGE_ERR);
    }
    
    //客户端必须要知道发给谁
    string serverip=argv[1];

    unique_ptr<icmpClient> ucli(new icmpClient(serverip));
    ucli->initClient();
    ucli->run();
    return 0;
}