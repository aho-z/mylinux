#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#define FILE_NAME(number) "log.txt"#number

// 每一个宏，对应的数值，只有一个比特位是1,彼此位置不重叠
//#define ONE (1<<0)
//#define TWO (1<<1)
//#define THREE  (1<<2)
//#define FOUR (1<<3)
//
//void show(int flags)
//{
//    if(flags & ONE) printf("one\n");
//    if(flags & TWO) printf("two\n");
//    if(flags & THREE) printf("three\n");
//    if(flags & FOUR) printf("four\n");
//}

int main()
{
  umask(0);//清除系统默认掩码
  
  printf("stdin->fd=%d\n",stdin->_fileno); 
  printf("stdout->fd=%d\n",stdout->_fileno); 
  printf("stderr->fd=%d\n",stderr->_fileno); 
  int fd0=open(FILE_NAME(1),O_WRONLY | O_CREAT|O_APPEND,0666); 
  int fd1=open(FILE_NAME(2),O_WRONLY | O_CREAT|O_APPEND,0666); 
  int fd2=open(FILE_NAME(3),O_WRONLY | O_CREAT|O_APPEND,0666); 
  
  printf("fd0=%d\n",fd0); 
  printf("fd1=%d\n",fd1); 
  printf("fd2=%d\n",fd2); 
  
  
  close(fd0); 
  close(fd1); 
  close(fd2); 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  //int fd=open(FILE_NAME,O_WRONLY);//打开文件不存在
  //int fd=open(FILE_NAME,O_WRONLY|O_CREAT);//文件属性错误
  //int fd=open(FILE_NAME,O_WRONLY|O_CREAT,0666);//再次写入的时候，没有对文件清空
  
  
  //c语言传W,意味着底层要传，
  // "w" -> O_WRONLY（写入） | O_CREAT（没有文件的时候创建） | O_TRUNC（有内容的时候清空）, 0666（属性）, O_APPEND（追加）O_RDONLY（读）
  // "a"
 // int fd=open(FILE_NAME,O_WRONLY|O_CREAT|O_TRUNC,0666);
 //int fd=open(FILE_NAME,O_RDONLY);
 // if(fd<0)
 // {
 //   perror("open");
 //   return 1;
 // }
 // char buffer[1024];
 // ssize_t num=read(fd,buffer,sizeof(buffer)-1);
 // if(num>0) buffer[num]=0;//要用C读，需要在字符串末尾加\0
 // printf("%s",buffer);

 // int cnt=5;
 // char  outbuffer[64];
 // while(cnt)
 // {
 //   sprintf(outbuffer,"%s:%d\n","abfcv",cnt--);
 //   //写入的时候不用strlen()+1，c规定的语法不影响文件
 //    write(fd,outbuffer,strlen(outbuffer));
 // }
 // printf("fd=%d\n",fd);
 // close(fd);










//    show(ONE);
//    printf("-----------------------\n");
//    show(TWO);
//    printf("-----------------------\n");
//    show(ONE | TWO);
//    printf("-----------------------\n");
//    show(ONE | TWO | THREE);
//    printf("-----------------------\n");
//    show(ONE | TWO | THREE | FOUR);
//	  printf("-----------------------\n");






  //r,w,r+(读写，不存在文件的时候报错)，w+(读写，不存在创建)，a(append,追加)，a+(在文件末尾进行读写)
  //FILE *fp=fopen(FILE_NAME,"w");
 // FILE *fp=fopen(FILE_NAME,"r");
 // 
 // if(fp==NULL)
 // {
 //   perror("fopen");
 //   return 1;
 // }
 // //读操作
 // char  buffer[64];
 // while(fgets(buffer,sizeof(buffer)-1,fp)!=NULL)
 // {
 //     buffer[strlen(buffer)-1]=0;
  //    puts(buffer);
 // }

  //写操作
 // int cnt=5;
 // while(cnt)
 // {
 //     fprintf(fp,"%s:%d\n","myfile.c",cnt--);
 // }
 // fclose(fp);

  return 0;
}
