#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <poll.h>
#include "sock.hpp"

namespace PoolServer_wmh
{
    static const int defaultport = 8083;
    static const int num = 2048; // pollfd数组大小
    static const int defaultfd = -1;

    using func_t = std::function<std::string(const std::string &)>;

    class PollServer
    {

    public:
        PollServer(func_t f, int port = defaultport)
            :_func(f), _port(port), _listensock(-1), rfds(nullptr)
        {
        }
        void ResetItem(int i)//初始化pollfd类型
        {
            rfds[i].fd = defaultfd;
            rfds[i].events = 0;
            rfds[i].revents= 0;
        }
        void initServer()
        {
            _listensock = Sock::CreateSocket();
            Sock::Bind(_listensock, _port);
            Sock::Listen(_listensock);
            // 初始化文件描述符数组
            rfds = new struct pollfd[num];
            for (int i = 0; i < num; ++i) ResetItem(i);
            // 目前只有它，先把listensock存进去
            rfds[0].fd = _listensock;
            rfds[0].events=POLLIN;
            
        }
        void Print()
        {
            std::cout << "fd list: ";
            for (int i = 0; i < num; i++)
            {
                if (rfds[i].fd != defaultfd)
                    std::cout << rfds[i].fd << " ";
            }
            std::cout << std::endl;
        }

        void Accepter(int _listensock)
        {
            logMessage(DEBUG, "Accepter in");
            std::string clientip;
            uint16_t clientport = 0;
            int sock = Sock::Accept(_listensock, &clientip, &clientport); // accept = 等 + 获取
            if (sock < 0)
                return;
            logMessage(NORMAL, "accept success [%s:%d]", clientip.c_str(), clientport);
            // 这里不能直接recv/read，只有select有资格检测时间按是否就绪
            // 需要将新的sock交给select
            // 本质就是添加到fdarray数组
            int i = 0;
            for (; i < num; ++i) // 找到空的的位置
            {
                if (rfds[i].fd != defaultfd)
                    continue;
                else
                    break;
            }
            if (i == num) // 数据已满
            {
                logMessage(WARNING, "server if full, please wait");
                close(sock);
            }
            else
            {
                rfds[i].fd = sock; // fd设置进数组
                rfds[i].events = POLLIN;
                rfds[i].revents = 0;
            }
            Print();
            logMessage(DEBUG, "Accepter out");
        }

        void Recver(int pos)
        {
            logMessage(DEBUG, "in recver");

            // 1.读取request，有bug
            char buffer[1024];
            ssize_t s = recv(rfds[pos].fd, buffer, sizeof(buffer) - 1, 0); // 不会被阻塞
            if (s > 0)
            {
                buffer[s] = 0;
                logMessage(NORMAL, "client# %s", buffer);
            }
            else if (s == 0)//客户端退出
            {
                close(rfds[pos].fd);
                ResetItem(pos);
                logMessage(NORMAL, "client quit");
                return;
            }
            else
            {
                close(rfds[pos].fd);
                ResetItem(pos);
                logMessage(ERROR, "client quit: %s", strerror(errno));
                return;
            }
            // 2. 处理request
            std::string response = _func(buffer);

            // 3. 返回response
            write(rfds[pos].fd, response.c_str(), response.size()); // 有bug

            logMessage(DEBUG, "out Recver");
        }

        void HandlerReadEvent()
        {
            for (int i = 0; i < num; ++i)
            {
                // 过滤非法的fd
                if (rfds[i].fd == defaultfd)
                    continue;
                //检测是否为读事件    
                if(!(rfds[i].events & POLLIN)) continue;
                // 正常的
                if (rfds[i].fd== _listensock && (rfds[i].revents & POLLIN)) 
                {
                    Accepter(_listensock);
                }
                else if (rfds[i].revents & POLLIN) 
                {
                    Recver(i);
                }
                else
                {
                }
            }
        }

        
        void start()
        {
            int timeout=-1;
            for (;;)
            {
                int n = poll(rfds,num,timeout);// 阻塞
                switch (n)
                {
                case 0: // 超时没有返回
                    logMessage(NORMAL, "timeout...");
                    break;
                case -1: // 错误
                    logMessage(WARNING, "select error, code: %d, err string: %s", errno, strerror(errno));
                    break;

                default:
                    // 说明有事件就绪了
                    logMessage(NORMAL, "have event ready!");
                    HandlerReadEvent();
                    break;
                }
            }
        }
        ~PollServer()
        {
            if (_listensock < 0)
                close(_listensock);
            if (rfds)
                delete[] rfds;
        }

    private:
        int _port;
        int _listensock;
        struct pollfd* rfds;
        func_t _func;
    };
}