#include "pollServer.hpp"
#include "errno.hpp"
#include <memory>

using namespace std;
using namespace PoolServer_wmh;

static void usage(std::string proc)
{
    std::cerr << "Usage:\n\t" << proc << " port" << "\n\n";
}

std::string transaction(const std::string &request)
{
    return request;
}

// ./select_server 8083
int main(int argc, char *argv[])
{
    unique_ptr<PollServer> svr(new PollServer(transaction));

    svr->initServer();

    svr->start();

    return 0;
}