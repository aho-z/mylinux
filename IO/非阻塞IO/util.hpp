#pragma once

#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <cerrno>


//将文件描述符设置为非阻塞
// 使用F_GETFL将当前的文件描述符的属性取出来(这是一个位图).
// 然后再使用F_SETFL将文件描述符设置回去. 
// 设置回去的同时, 加上一个O_NONBLOCK参数.
void setNonBlock(int fd)
{
    //获得文件状态标记
    int f1=fcntl(fd,F_GETFL);
    if(f1<0)
    {
        std::cerr<<"fcntl: "<<strerror(errno)<<std::endl;
        return;
    }
    //设置文件描述符标记
    fcntl(fd,F_SETFL,f1|O_NONBLOCK);
}

void printLog()
{
    std::cout << "this is a log" << std::endl;
}

void download()
{
    std::cout << "this is a download" << std::endl;
}

void executeSql()
{
    std::cout << "this is a executeSql" << std::endl;
}