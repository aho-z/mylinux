#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <sys/epoll.h>

#include "sock.hpp"

namespace EpoolServer_wmh
{
    static const int defaultport = 8083;
    static const int size = 128;
    static const int defaultvalue = -1;
    static const int defalultnum = 64;

    using func_t = std::function<std::string(const std::string &)>;
    class EpollServer
    {

    public:
        EpollServer(func_t f, uint16_t port = defaultport, int num = defalultnum)
            : _func(f), _num(num), _revs(nullptr), _port(port), _listensock(defaultvalue), _epfd(defaultvalue)
        {
        }

        void initServer()
        {
            _listensock = Sock::CreateSocket();
            Sock::Bind(_listensock, _port);
            Sock::Listen(_listensock);
            // 创建epoll模型
            _epfd = epoll_create(size);
            if (_epfd < 0)
            {
                logMessage(FATAL, "epoll create error:%s", strerror(errno));
                exit(EPOLL_CREATE_ERR);
            }

            // 添加listensock到epoll中
            struct epoll_event ev;
            ev.events = EPOLLIN;                               // 添加读事件
            ev.data.fd = _listensock;                          // 添加要关心的文件描述符
            epoll_ctl(_epfd, EPOLL_CTL_ADD, _listensock, &ev); // 添加事件

            // 4.申请就绪事件空间
            _revs = new struct epoll_event[_num];
            logMessage(NORMAL, "init server success");
        }

        // readyNum就绪事件的大小
        void HandlerReadEvent(int readyNum)
        {
            logMessage(DEBUG, "HandlerReadEvent in");
            for (int i = 0; i < readyNum; i++)
            {
                uint32_t events = _revs[i].events;
                int sock = _revs[i].data.fd;
                if (sock == _listensock && (events & EPOLLIN))
                {
                    // listensock读事件就绪，获取新链接
                    std::string clientip;
                    uint16_t clientport;
                    int fd = Sock::Accept(sock, &clientip, &clientport);
                    if (fd < 0)
                    {
                        logMessage(WARNING, "accept error");
                        continue;
                    }
                    // 获取fd成功，需要先将其放入epoll中，再进行读取
                    struct epoll_event ev;
                    ev.data.fd = fd;
                    ev.events = EPOLLIN;
                    epoll_ctl(_epfd, EPOLL_CTL_ADD, fd, &ev);
                }
                else if (events & EPOLLIN)
                {
                    // 普通读事件就绪，有bug，不能完全读完
                    char buffer[1024];
                    int n = recv(sock, buffer, sizeof(buffer), 0);
                    if (n > 0)
                    {
                        buffer[n] = 0;
                        logMessage(DEBUG, "client# %s", buffer);
                        std::string response = _func(buffer);
                        send(sock, response.c_str(), response.size(), 0);
                    }
                    else if (n == 0)
                    {
                        // 先从epoll移除，再关闭
                        epoll_ctl(_epfd, EPOLL_CTL_DEL, sock, nullptr);
                        close(sock);
                        logMessage(NORMAL, "client quit");
                    }
                    else
                    {
                        // 先从epoll移除，再关闭
                        epoll_ctl(_epfd, EPOLL_CTL_DEL, sock, nullptr);
                        close(sock);
                        logMessage(ERROR, "recv error, code: %d, errstring: %s", errno, strerror(errno));
                    }
                }
                else
                {
                }
            }
            logMessage(DEBUG, "HandlerReadEvent out");
        }

        void start()
        {
            int timeout = -1;
            for (;;)
            {
                int n = epoll_wait(_epfd, _revs, _num, timeout);
                switch (n)
                {
                case 0:
                    logMessage(NORMAL, "timeout ...");
                    break;
                case -1:
                    logMessage(WARNING, "epoll_wait failed, code: %d, errstring: %s", errno, strerror(errno));
                    break;
                default:
                    logMessage(NORMAL, "have event ready");
                    HandlerReadEvent(n);
                    break;
                }
            }
        }
        ~EpollServer()
        {
            if (_listensock != defaultvalue)
                close(_listensock);
            if (_epfd != defaultvalue)
                close(_epfd);
            if (_revs)
                delete[] _revs;
        }

    private:
        uint16_t _port;
        int _listensock;
        int _epfd;
        struct epoll_event *_revs;
        int _num;
        func_t _func;
    };
}