#include <string.h>
#include "progress.h"
#include <unistd.h>

//进度条大小
#define SIZE 102
//进度条样式
#define STYLE '='
//进度条指示器
#define S '>'

void show(){
  const char* tmp="|/-\\";
  char arr[SIZE];
  memset(arr,0,sizeof(arr));
  int i=0;
  while(i<=100){
     printf("[\033[43;31;1m%-100s\033[0m]][%d%%][%c]\r",arr,i,tmp[i%4]);
      fflush(stdout);
      arr[i++]=STYLE;
      if(i != 100) arr[i]=S;
      usleep(100000);
    
  }
  printf("\n");


}
