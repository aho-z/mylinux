#pragma once
#include <iostream>
#include <fstream>
#include <string>
class Util
{
public:
    static std::string getOneLine(std::string &buffer,const std::string& sep )
    {
        auto pos=buffer.find(sep);
        if(pos==std::string::npos) return "";
        std::string sub=buffer.substr(0,pos);
        buffer.erase(0,sub.size()+sep.size());
        return sub;
    }
    // static bool readFile(const std::string resourse,std::string* out)
    // {
    //     std::ifstream in(resourse);
    //     if(!in.is_open()) return false;

    //     std::string line;
    //     while (std::getline(in,line))
    //     {
    //         *out+=line;
    //     }
    //     in.close();
    //     return true;

    // }

    static bool readFile(const std::string resourse,char* buffer,int size)
    {
        std::ifstream in(resourse,std::ios::binary);
        if(!in.is_open()) return false;

        in.read(buffer,size);
        in.close();
        return true;

    }
};