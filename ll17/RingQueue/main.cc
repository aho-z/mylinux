#include "RingQueue.hpp"
#include "Task.hpp"
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

using std::cout;
using std::endl;

std::string SelfName()
{
    char name[128];
    snprintf(name,sizeof name,"thread[0x%x]",pthread_self());
    return name;
}

void* ProductorRoutine(void* args)
{
    RingQueue<Task>* rq=static_cast<RingQueue<Task>*>(args);
    while (true)
    {
        //任务1
        // int data=rand()%10+1;
        // rq->push(data);
        // cout<<"生产完成"<<data<<endl;
        // sleep(1);

        //任务2
        //获取任务
        int x=rand() % 10 ;
        int y=rand() % 5;
        char op=oper[rand()%oper.size()];
        Task t(x,y,op,mymath);
        //生产任务
        rq->push(t);
        //输出提示
        cout<< SelfName()<<",派发了一个任务: "<<t.toTaskString()<<endl;
        sleep(1);
    }
    
}
void* ComsumerRoutine(void* args)
{
    RingQueue<Task>* rq=static_cast<RingQueue<Task>*>(args);
    while (true)
    {
        //任务1
        // int data;
        // rq->pop(&data);
        // cout<<"消费完成"<<data<<endl;

        //任务2
        Task t;
        //消费任务
        rq->pop(&t);
        std::string result=t();
        cout<< SelfName()<<",消费了一个任务: "<<result<<endl;

    }
    
}


int main()
{
    srand((size_t)time(0)^getpid()^0x22113121);
    RingQueue<Task>* rq=new RingQueue<Task>();
    // pthread_t c,p;
    // pthread_create(&p,nullptr,ProductorRoutine,rq);
    // pthread_create(&c,nullptr,ComsumerRoutine,rq);

    // pthread_join(p,nullptr);
    // pthread_join(c,nullptr);

    //多生产多消费
    pthread_t p[4],c[8];
    for(int i=0;i<4;i++) pthread_create(p+i,nullptr,ProductorRoutine,rq);
    for(int i=0;i<8;i++) pthread_create(c+i,nullptr,ComsumerRoutine,rq);
    
    
    for(int i=0;i<4;i++) pthread_join(p[i],nullptr);
    for(int i=0;i<8;i++) pthread_join(c[i],nullptr);

    delete rq;

    return 0;
}