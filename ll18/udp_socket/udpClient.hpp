#pragma once

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
namespace Client
{
    using namespace std;

    // //默认的点分十进制IP
    // static const string defaultIP="0.0.0.0";
    // static const int gnum=1024;
    
    enum{USAGE_ERR=1,SOCKET_ERR,BIND_ERR};//退出码

    class udpClient
    {
    public:
        udpClient(const string& serverip,const uint16_t&serverport)
            :serverip_(serverip),serverport_(serverport),sockfd_(-1),quit_(false)
        {}
        void initClient()
        {
            //****1.创建UDP网络通信端口****
            sockfd_=socket(AF_INET,SOCK_DGRAM,0);
            if(sockfd_==-1)
            {
                cerr << "socket error: " << errno << " : " << strerror(errno) << endl;
                exit(SOCKET_ERR);
            }
            cout << "socket success: " << " : " << sockfd_ << endl;

            //****Client必须要bind，但是Client不需要显示bind（不需要自己写）******/
            //客户端端口是多少不重要，只要能保证唯一性就行，让OS自己去绑
        }

        void run()
        {
            /*********发送数据***********/
            struct sockaddr_in server;
            memset(&server,0,sizeof(server));
            server.sin_family=AF_INET;
            //主机序列转网络序列
            server.sin_addr.s_addr=inet_addr(serverip_.c_str());
            server.sin_port=htons(serverport_);

            string message;
            while (!quit_) 
            {
                cout<<"Please Enter# "<<endl;
                cin>>message;
                //首次向服务器发送数据的时候，OS识别到还未绑定，sendto自动绑定ip+port
                sendto(sockfd_,message.c_str(),message.size(),0,(struct sockaddr*)&server,sizeof(server));
            }
            
        }

        ~udpClient()
        {}
    private:
        int sockfd_;
        string serverip_;
        uint16_t serverport_;
        bool quit_;
    };
}
