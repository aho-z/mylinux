#include "comm.hpp"

int main()
{
    //先运行此文件，由于文件还未创建，无法写入
    std::cout << "client begin" << std::endl;
    int wfd=open(NAMED_PIPE,O_WRONLY);
    std::cout<< "client end" <<std::endl;
    if(wfd<0) exit(1);

    //write
    char buffer[1024];
    while (true)
    {
        std::cout<<"Please Say#";
        fgets(buffer,sizeof buffer,stdin);
        if(strlen(buffer)>0)
            buffer[strlen(buffer)-1]=0;
        ssize_t n=write(wfd,buffer,strlen(buffer));
        assert(n==strlen(buffer));
        (void)n;    
    }
    close(wfd);
    return 0;
    
}