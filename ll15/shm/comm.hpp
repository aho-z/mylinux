#ifndef _COMM_HPP_
#define _COMM_HPP_

#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <sys/ipc.h>
#include <sys/shm.h>
#define PATHNAME "."//共享内存路径
#define PROJ_ID 0x66 //八位非零有效数字
//系统分配共享内存是以4KB为单位的,如果是4097，内核会向上取整
#define MAX_SIZE 4096

key_t getKey()
{
    //能够唯一标识一块内存，设置进共享内存属性中，保证该共享内存在内核中的唯一性
    key_t k= ftok(PATHNAME,PROJ_ID);
    if(k<0)
    {
        //cin,cout,cerr对应stdin,stdout,stderr，0,1,2
        //标准错误输出流，输出当前发生的错误信息
        std::cerr<<errno<<":"<<strerror(errno)<<std::endl;
        exit(-1);
    }
    return k;
}

//IPC_CREAT不存在shm就创建，存在就获取
//IPC_EXCL|IPC_CREAT 不存在创建，存在出错返回，保证新创建的是新的shm
int getShmHelper(key_t k,int flags)
{
    //shmid和k  类似于于锁和钥匙的关系，
    //相当于现实世界中学号和身份证号，都是指定某一个人，为了解耦，用两个标识
    //应用层shmid,系统调用用K，即便系统调用改变，也不影响上层使用
    //shmid要把k设置进共享内存属性中，用来在内核中标识唯一性
    int shmid=shmget(k,MAX_SIZE,flags);
    if(shmid<0)
    {
        std::cerr<<errno<<":"<<strerror(errno)<<std::endl;
        exit(2);
    }
    return shmid;
}
int getShm(key_t k)//获取shm
{
    return getShmHelper(k,IPC_CREAT);
}
int CreateShm(key_t k)//创建shm
{
    //0600共享内存权限
    return getShmHelper(k,IPC_CREAT|IPC_EXCL|0600);
}

//shm与进程关联
void* attachShm(int shmid)
{
    void * mem = shmat(shmid,nullptr,0);//linux下指针占8个字节
    if((long long)mem==-1L)
    {
        std::cerr<<errno<<":"<<strerror(errno)<<std::endl;
        exit(3);
    }
    return mem;
}
//解除关联
void detachShm(void* start)
{
    if(shmdt(start)==-1)
    {
        std::cerr<<errno<<":"<<strerror(errno)<<std::endl;
    }
}
//删除共享空间
void delShm(int shmid)
{
    //IPC_PMID立即删除共享内存  
    if(shmctl(shmid,IPC_RMID,nullptr)==-1)
    {
        std::cerr<<errno<<":"<<strerror(errno)<<std::endl;
    }
}
#endif