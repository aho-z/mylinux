#include "comm.hpp"
#include <unistd.h>

//由于此文件负责创建shm，一定要先运行
int main()
{
    key_t k=getKey();
    printf("key: 0x%x\n",k);

    int shmid=getShm(k);
    printf("shmid: %d\n",shmid);

    //sleep(5);

    //关联shm
    char* start=(char*)attachShm(shmid);
    printf("attach success,address start:%p\n",start);


    //使用
    const char* str="我是client,我正在给你通信";
    pid_t id=getpid();
    int cnt=1;
    while (true)
    {
        sleep(3);
        //不需要建立缓冲区，直接往shm里面拷贝数据
        snprintf(start,MAX_SIZE,"%s[my_pid:%d][消息编号:%d]",str,id,cnt++);
        // snprintf(buffer, sizeof(buffer), "%s[pid:%d][消息编号:%d]", message, id, cnt++);
        // memcpy(start, buffer, strlen(buffer)+1);
    }
    
    //取消关联
    detachShm(start);
    return 0;

}