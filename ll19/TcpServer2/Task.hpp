#pragma once

#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <functional>

void serverIO(int sock)
{
    char buffer[1024];
    while (true)
    {
        ssize_t n = read(sock, buffer, sizeof(buffer) - 1);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "recv message " << buffer << std::endl;
            std::string outbuffer = buffer;
            outbuffer += "server[echo]";
            write(sock, outbuffer.c_str(), outbuffer.size());
        }
        else if (n == 0)
        {
            // 客户端退出
            logMessage(NORMAL, "client quit ,me too!!");
            break;
        }
    }
    close(sock);
}
class Task // 计算任务
{

public:
    using func_t = std::function<void(int)>;
    Task() {}
    Task(int sock, func_t callback)
        : sock_(sock), callback_(callback)
    {
    }
    void operator()()
    {
        callback_(sock_);
    }

private:
    int sock_;
    func_t callback_;
};

