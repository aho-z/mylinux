#pragma once
#include <iostream>
#include <string>
#include <stdarg.h>
#include <ctime>
#include <unistd.h>

//把错误信息写到指定文件

#define LOG_NORMAL "log_NRL.txt"
#define LOG_ERR "log_err.txt"



#define DEBUG   0
#define NORMAL  1
#define WARNING 2
#define ERROR   3
#define FATAL   4

//typedef char* va_list

const char * to_levelstr(int level)
{
    switch(level)
    {
        case DEBUG : return "DEBUG";
        case NORMAL: return "NORMAL";
        case WARNING: return "WARNING";
        case ERROR: return "ERROR";
        case FATAL: return "FATAL";
        default : return nullptr;
    }
}

//void logMessage(DEBUG,"sss %f %d %c",2.1,6,'h')
void logMessage(int level,const char* format,...)
{
    //[日志等级][时间][pid][message]
    // va_list start;

    //得到第一个可变参数的地址
    // va_start(start);
    // //让指针移动指定大小
    // va_arg(start,int);
    // //让指针为空
    // va_end(start);

#define NUM 1024
    char logprefix[NUM];

    //前半部分[日志等级][时间][pid]
    snprintf(logprefix,sizeof(logprefix),"[%s][%ld][%d]",to_levelstr(level),(long int)time(nullptr),getpid());

    char logcontent[NUM];
    va_list arg;
    va_start(arg,format);//得到第一个可变参数的地址,arg就指向format

    //后半部分，错误信息
    vsnprintf(logcontent,sizeof(logcontent),format,arg);  //将可变参数列表的起始部分以特定的格式写入到缓冲区

    //std::cout<<logprefix<<logcontent<<std::endl;  

    FILE* log=fopen(LOG_NORMAL,"a");
    FILE* err=fopen(LOG_ERR,"a");

    if(log !=nullptr && err!=nullptr)
    {
        //FILE* cur=nullptr;
        if(level==DEBUG || level==NORMAL||level==WARNING)
        {
            fprintf(log,"%s%s\n",logprefix,logcontent);
        }

        if(level==ERROR || level==FATAL)
        {
            fprintf(err,"%s%s\n",logprefix,logcontent);
        }

        fclose(log);
        fclose(err);
    }

}