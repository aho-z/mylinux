#pragma once
#include <unistd.h>
#include <signal.h>
#include <cstdlib>
#include <cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEV "/dev/null"  //垃圾站，写进的内容全丢弃



//自建一个会话，组长就是自己，调用函数的不能是组长
//调用完毕之后成为组长


void daemonSelf(char* currPath=nullptr)
{
    //1.让调用进程忽略掉异常的信号
    //客户端已经退出，服务端再写会崩溃
    signal(SIGPIPE,SIG_IGN);

    //2.不是组长，调用setsid
    if(fork()>0) exit(0);
    
    //子进程---守护进程又叫精灵进程---本质孤儿进程
    pid_t n=setsid();
    assert(n!=-1);

    //3.守护进程是脱离终端的，关闭或重定向以前进程默认打开的文件

    //      /dev/null 垃圾站，写进的内容全丢弃
    int fd=open(DEV,O_RDWR);
    if(fd>=0)
    {
        dup2(fd,0);
        dup2(fd,1);
        dup2(fd,2);

        //012已经执行devil/null
        close(fd);
    }
    else
    {
        close(0);
        close(1);
        close(2);
    }

    //4.可选：进程执行路径发生更改
    //chdir：将进程的当前工作目录更改为 path 参数指定的目录
    if(!currPath) chdir(currPath);


}