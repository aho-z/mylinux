#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>
int main(int argc,char*argv[])
{
  printf("process is running...\n");
  pid_t id=fork();
  assert(id != -1);
  if(id==0)
  {
    
    sleep(1);
    //命令行参数替换
    //./myexec ls -a -l-->./myexec "ls" "-a"  只需要指向ls及其之后的指令
    //&argv[1]作为参数传递给execvp函数，以便在新程序中使用除了第一个元素以外的所有参数。
    execvp(argv[1],&argv[1]);


//   //execl("/usr/bin/ls","ls","-a","-l","--color=auto",NULL);
//   //execlp("ls","ls","-a","-l","--color=auto",NULL);//不需要指定具体路径，自动到path中去找
//    
//    char* const argv_[]={
//      "ls","-a","-l","--color=auto",NULL
//    };
//    //execv("/usr/bin/ls",argv_);
//    //execvp("ls",argv_);
//    //execl("./myotherc","myotherc",NULL);
//    
//    
//    char* const envp_[]={
//      (char*)"MYENV=33724672",
//      NULL
//    };
//   
//    
//    //execle("./myotherc","myotherc",NULL,envp_);这种调用方法尽可显示自定义变量
//   extern char** environ; 
//   putenv((char*)"MYENV=33724673");//将指定环境变量导入到系统中，environ指向的环境变量表中
//    execle("./myotherc","myotherc",NULL,environ);//默认环境变量不传，子进程也能获取
    exit(-1);

  }
  int status=0;
  pid_t ret=waitpid(id,&status,0);
  if(ret>0) printf("wait success:exit code:%d,sig:%d\n",(status>>8)&0xFF,status&0x7F);





















  //////.c->exe(可执行文件)->load(被系统加载)->process(进程)->执行代码
  //printf("process is running...\n");
  ////load->exe将程序加载到内存
  ////函数存在调用失败的情况，失败就不会进行替换，后面代码也会去执行
  ////execl不需要成功调用的返回值，成功之后与下面代码无关，判断无意义
  ////execl只要返回了，一定是错误了
  //execl("/usr/bin/lsadadas","ls","--color=auto","-a","-l",NULL);//所有的execl参数都必须以NUll结束
  //perror("execl");

  ////execl函数调用成功的时候，代码已经全部被替换，后面代码就不会执行了
  //printf("process is done...\n");
  //exit(1);
}

