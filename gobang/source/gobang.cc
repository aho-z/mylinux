#include "server.hpp"
#include <iostream>
#define HOST "127.0.0.1"
#define USER "root"
#define PASS ""
#define DBNAME "gobang"
#define PORT 3306

int mysql_test()
{

    // ILOG("I日志宏函数");
    // DLOG("D日志宏函数");
    // ELOG("E日志宏函数");
    MYSQL* mysql=mysql_util::mysql_create(HOST,USER,PASS,DBNAME,PORT);
    const char*sql="insert stu values(null,'狗子',18,50,60,70);";
    bool ret=mysql_util::mysql_exec(mysql,sql);
    if(ret!=0)
    {
        return -1;
    }
    mysql_util::mysql_destroy(mysql);
    return 0;
}

int json_tets()
{
    Json::Value root;
    std::string body;
    root["姓名"]="张三";
    root["年龄"]=18;
    root["成绩"].append(100);
    root["成绩"].append(60);
    root["成绩"].append(50);
    json_util::serialize(root,body);
    DLOG("%s",body.c_str());

    Json::Value val;
    json_util::unserialize(body,val);
    //逐个提取json::value中的数据
    std::cout<<"姓名："<<root["姓名"].asCString()<<std::endl;
    std::cout<<"年龄："<<root["年龄"].asInt()<<std::endl;
    int sz=root["成绩"].size();
    for(int i=0;i<sz;++i)
    {
        std::cout<<"成绩： "<<root["成绩"][i].asFloat()<<std::endl;
    }


}
void str_test()
{
    std::string str="......**ssss*sss*s**222**ss";
    std::vector<std::string> vs;
    string_util::split(str,"*",vs);
    for(auto& s:vs)
    {
        DLOG("%s",s.c_str());
    }
}

void file_test()
{
    std::string filename="./makefile";
    std::string body;
    file_util::read(filename,body);
    DLOG("%s",body.c_str());
}

void dbtest()
{
    user_table ut(HOST,USER,PASS,DBNAME,PORT);
    Json::Value user;
    user["username"]="张三";
    //user["password"]="111211";
    ut.insert(user);
    // bool ret=ut.login(user);
    // if(ret==false)
    //     DLOG("LOGIN FAILED");
    // bool ret=ut.select_by_id(1,user);
    // std::string body;
    // json_util::serialize(user,body);
    // DLOG("body:%s",body.c_str());

    //bool ret=ut.lose(1);
}

void online_test()
{
    online_manager om;
    wsserver_t::connection_ptr conn;
    uint64_t uid=2;
    om.enter_game_room(uid,conn);
    if(om.is_in_game_room(uid)){
        DLOG("IN GAME HALL");
    }else{
        DLOG("NOT IN GAME HALL");
    }
    om.exit_game_room(uid);
    if(om.is_in_game_room(uid)){
        DLOG("IN GAME HALL");
    }else{
        DLOG("NOT IN GAME HALL");
    }
}

int main()
{
    // file_test();

    //dbtest();
   //online_test();
//    user_table ut(HOST,USER,PASS,DBNAME,PORT);
//    online_manager om;
//    room_manager rm(&ut,&om);
//    room_ptr rp=rm.create_room(10,20);

//    matcher mc(&rm,&ut,&om);
    gobang_server server(HOST,USER,PASS,DBNAME,PORT);
    server.start(8080);
    return 0;
}