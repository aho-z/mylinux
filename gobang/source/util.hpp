#ifndef __M_UTIL_H__
#define __M_UTIL_H__

#include "logger.hpp"
#include <mysql/mysql.h>
#include <string>
#include <fstream>

#include <memory>
#include <vector>
#include <sstream>
#include <string>
#include <jsoncpp/json/json.h>
#include "logger.hpp"

class mysql_util
{
public:
    //建立连接
    static MYSQL *mysql_create(const std::string &host,
                               const std::string &username,
                               const std::string &password,
                               const std::string &dbname,
                               uint16_t port = 3306)
    {
        // 1.初始化mysql句柄
        // MYSQL *mysql_init(MYSQL *mysql)
        // mysql为空则动态申请句柄空间进⾏初始化
        MYSQL *mysql = mysql_init(NULL);
        if (mysql == NULL)
        {
            ELOG("mysql init failed!");
            return NULL;
        }
        // 2.连接服务器
        if (mysql_real_connect(mysql, host.c_str(), username.c_str(), password.c_str(), dbname.c_str(), port, NULL, 0) == NULL)
        {
            ELOG("connect mysql server failed:%s", mysql_error(mysql));
            mysql_close(mysql); // 释放句柄
            return NULL;
        }

        // 3.设置客户端字符集
        if (mysql_set_character_set(mysql, "utf8") != 0)
        {
            ELOG("set client character failed:%s", mysql_error(mysql));
            mysql_close(mysql);
            return NULL;
        }

        return mysql;
    }

    //执行sql语句
    static bool mysql_exec(MYSQL *mysql, const std::string &sql)
    {
        int ret=mysql_query(mysql,sql.c_str());
        if(ret!=0){
            ELOG("%s\n",sql.c_str());
            ELOG("msyql query failed:%s\n",mysql_error(mysql));
            //mysql_close(mysql);
            return false;
        }

        return true;
    }


    //关闭连接
    static void mysql_destroy(MYSQL *mysql)
    {
        if(mysql!=NULL)
        {
            mysql_close(mysql);
        }
        return;
    }
};

class json_util
{
public:
    static bool serialize(const Json::Value &root, std::string &str)
    {
        // 1.实例化一个StreamWriterBuilder工厂类对象
        Json::StreamWriterBuilder swb;
        // 2.通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
        std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
        // 3.使用StreamWriter对象，对Json::value中存储的数据进行序列化
        std::stringstream ss;
        int ret = sw->write(root, &ss);
        if (ret != 0)
        {
            ELOG("json serialize failed!");
            return false;
        }
        str = ss.str();

        return true;
    }

    static bool unserialize(const std::string &str, Json::Value &root)
    {
        // 1.实例化一个charReaderBuilder工厂类对象
        Json::CharReaderBuilder crd;
        // 2.使用工厂类对象生产一个CharReader对象
        std::unique_ptr<Json::CharReader> cr(crd.newCharReader());

        std::string err;
        // 3.使用charReader对象进行json格式字符串str的反序列化
        bool ret = cr->parse(str.c_str(), str.c_str() + str.size(), &root, &err);
        if (ret == false)
        {
            ELOG("json unserialize failed! %s", err.c_str());
            return false;
        }

        // Json::Value root;
        // Json::Reader reder;
        // reder.parse(str,root);
        // std::cout<<"姓名："<<root["姓名"].asCString()<<std::endl;
        // std::cout<<"年龄："<<root["年龄"].asInt()<<std::endl;
        // int sz=root["成绩"].size();
        // for(int i=0;i<sz;++i)
        // {
        //     std::cout<<"成绩： "<<root["成绩"][i].asFloat()<<std::endl;
        // }

        return true;
    }
};

class string_util{
public:    
    static int split(const std::string& src,const std::string& sep,std::vector<std::string>& res)
    {
        //ssss*sss*s****ss;
        size_t pos,index=0;
        while(index<src.size())
        {
            pos=src.find(sep,index);
            //最后一个子串
            if(pos==std::string::npos)
            {
                res.push_back(src.substr(index));
                break;
            }
            //考虑连续的分隔符情况
            if(index==pos)
            {
                index+=sep.size();
                continue;
            }

            res.push_back(src.substr(index,pos-index));
            index=pos+sep.size();
        }
        return res.size();
    }
};

class file_util{
public:
    static bool read(const std::string& fielname,std::string& body)
    {
        //1.以二进制读方式打开文件
        std::ifstream ifs(fielname,std::ios::binary);
        if(ifs.is_open()==false){
            ELOG("%s file open failed",fielname.c_str());
            return false;
        }
        //获取文件大小
        size_t fsize=0;
        //将文件指针移动到文件的末尾
        ifs.seekg(0,std::ios::end);
        //获取偏移量---文件大小
        fsize=ifs.tellg();
        //将文件指针移动到文件的开头
        ifs.seekg(0,std::ios::beg);
        body.resize(fsize);

        //将文件所有数据读出来
        //不能用body.c_str(),他的返回值是const类型
        ifs.read(&body[0],fsize);
        if(ifs.good()==false){//如果流状态不是good，表示异常
            ELOG("read %s file content failed!",fielname.c_str());
            ifs.close();
            return false;
        }

        //关闭文件
        ifs.close();
        return true;
    }
};


#endif