drop database if exists gobang;
create database if not exists gobang;
use gobang;

create table if not exists user(
    id int primary key auto_increment, /*主键，自增属性*/
    username varchar(32) unique key not null,
    password varchar(128) not null,
    score int,/*分数*/
    total_count int,/*对战场次*/
    win_count int/*胜利场次*/
);