#ifndef __M_STRINGUTIL_H__
#define __M_STRINGUTIL_H__
#include <string>
#include <vector>
class string_util{
public:    
    static int split(const std::string& src,const std::string& sep,std::vector<std::string>& res)
    {
        //ssss*sss*s****ss;
        size_t pos,index=0;
        while(index<src.size())
        {
            pos=src.find(sep,index);
            //最后一个子串
            if(pos==std::string::npos)
            {
                res.push_back(src.substr(index));
                break;
            }
            //考虑连续的分隔符情况
            if(index==pos)
            {
                index+=sep.size();
                continue;
            }

            res.push_back(src.substr(index,pos-index));
            index=pos+sep.size();
        }
        return res.size();
    }
};

#endif