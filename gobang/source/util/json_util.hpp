#ifndef __M_JSONUTIL_H__
#define __M_JSONUTIL_H__
#include <memory>
#include <vector>
#include <sstream>
#include <string>
#include <jsoncpp/json/json.h>
#include "logger.hpp"
class json_util
{
public:
    static bool serialize(const Json::Value &root, std::string &str)
    {
        // 1.实例化一个StreamWriterBuilder工厂类对象
        Json::StreamWriterBuilder swb;
        // 2.通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
        std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
        // 3.使用StreamWriter对象，对Json::value中存储的数据进行序列化
        std::stringstream ss;
        int ret = sw->write(root, &ss);
        if (ret != 0)
        {
            ELOG("json serialize failed!");

            return false;
        }
        str = ss.str();

        return true;
    }

    static bool unserialize(const std::string &str, Json::Value &root)
    {
        // 1.实例化一个charReaderBuilder工厂类对象
        Json::CharReaderBuilder crd;
        // 2.使用工厂类对象生产一个CharReader对象
        std::unique_ptr<Json::CharReader> cr(crd.newCharReader());

        std::string err;
        // 3.使用charReader对象进行json格式字符串str的反序列化
        bool ret = cr->parse(str.c_str(), str.c_str() + str.size(), &root, &err);
        if (ret == false)
        {
            ELOG("json unserialize failed! %s", err.c_str());
            return false;
        }

        // Json::Value root;
        // Json::Reader reder;
        // reder.parse(str,root);
        // std::cout<<"姓名："<<root["姓名"].asCString()<<std::endl;
        // std::cout<<"年龄："<<root["年龄"].asInt()<<std::endl;
        // int sz=root["成绩"].size();
        // for(int i=0;i<sz;++i)
        // {
        //     std::cout<<"成绩： "<<root["成绩"][i].asFloat()<<std::endl;
        // }

        return true;
    }
};

#endif