#include <stdio.h>

#include <string.h>
#include <mysql/mysql.h>

#define HOST "127.0.0.1"
#define USER "root"
#define PASS ""
#define DBNAME "gobang"
#define PORT 3306




int main()
{
    //1.初始化mysql句柄
    //MYSQL *mysql_init(MYSQL *mysql)
    //mysql为空则动态申请句柄空间进⾏初始化
    MYSQL* mysql=mysql_init(NULL);
    if(mysql==NULL){
        printf("mysql init failed!\n");
        return -1;
    }
    //2.连接服务器
    if(mysql_real_connect(mysql,HOST,USER,PASS,DBNAME,PORT,NULL,0)==NULL){
        printf("connect mysql server failed:%s\n",mysql_error(mysql));
        mysql_close(mysql);//释放句柄
        return -1;
    }

    // 3.设置客户端字符集
    if(mysql_set_character_set(mysql,"utf8")!=0)
    {
        printf("set client character failed:%s\n",mysql_error(mysql));
        mysql_close(mysql);
        return -1;
    }
    // 4.选择要操作的的数据库
    //链接服务器的时候已经设置过，不需要设置
    //mysql_select_db(mysql,DBNAME);

    // 5.执行sql语句
    //char* sql="insert stu values(null,'张三',18,50,60,70);";//增
    // char* sql="update stu set ch=ch+5 where sn=1";更新
    // char* sql="delete from stu where sn=1;";//删除
    char* sql ="select* from stu;";

    int ret=mysql_query(mysql,sql);
    if(ret!=0){
        printf("%s\n",sql);
        printf("msyql query failed:%s\n",mysql_error(mysql));
        mysql_close(mysql);
        return -1;
    }

    // 6.如果sql语句是查询语句，保存结果到本地
    MYSQL_RES* res=mysql_store_result(mysql);//保存结果到本地
    if(res==NULL){
        printf("mysql_store_result failed:%s\n",mysql_error(mysql));
        mysql_close(mysql);
        return -1;
    }
    //     6.1获取结果集中的结果条数
    int num_row=mysql_num_rows(res);//获取行数
    int num_col=mysql_num_fields(res);//获取列数
    //     6.2遍历保存到本地的结果集
    for(int i=0;i<num_row;++i)
    {
        //获取结果集下一行数据并保存到数组中，数组中每一个元素表示一列数据
        //MYSQL_ROW==char**
        MYSQL_ROW row=mysql_fetch_row(res);
        for(int i=0;i<num_col;++i)
        {
            printf("%s\t",row[i]);
        }
        printf("\n");
    }

    //     6.3释放结果集
    mysql_free_result(res);
    // 7.关闭连接，释放句柄
    mysql_close(mysql);


    return 0;


}