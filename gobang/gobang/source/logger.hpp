#ifndef __M_LOGGER_H__//防止被重复包含
#define __M_LOGGER_H__
#include<stdio.h>
#include<time.h>

#define INF 0 //提示型
#define DBG 1
#define ERR 2
#define DEFAULT_LOG_LEVEL DBG//默认日志等级
/* / 对换行符转义*/
#define LOG(level,format,...) do{\
    /*如果日志等级小于默认，不输出*/\
    if(DEFAULT_LOG_LEVEL>level) break;\
    time_t t=time(NULL);/*生成时间戳*/\
    struct tm*lt=localtime(&t);/*将时间戳转换为结构化日期*/\
    char buf[32]={0};\
    strftime(buf,31,"%H:%M:%S",lt);/*将结构化的日期转化成一行*/\
    fprintf(stdout,"[%s:%s:%d] " format "\n",buf,__FILE__,__LINE__,##__VA_ARGS__);\
}while(0)


#define ILOG(format,...) LOG(INF,format,##__VA_ARGS__)
#define DLOG(format,...) LOG(DBG,format,##__VA_ARGS__)
#define ELOG(format,...) LOG(ERR,format,##__VA_ARGS__)






#endif