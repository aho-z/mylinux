#include <iostream>
#include <fstream>
#include "logger.hpp"
class file_util{
public:
    static bool read(const std::string& fielname,std::string& body)
    {
        //1.以二进制读方式打开文件
        std::ifstream ifs(fielname,std::ios::binary);
        if(ifs.is_open()==false){
            ELOG("%s file open failed",fielname.c_str());
            return false;
        }
        //获取文件大小
        size_t fsize=0;
        //将文件指针移动到文件的末尾
        ifs.seekg(0,std::ios::end);
        //获取偏移量---文件大小
        fsize=ifs.tellg();
        //将文件指针移动到文件的开头
        ifs.seekg(0,std::ios::beg);
        body.resize(fsize);

        //将文件所有数据读出来
        //不能用body.c_str(),他的返回值是const类型
        ifs.read(&body[0],fsize);
        if(ifs.good()==false){//如果流状态不是good，表示异常
            ELOG("read %s file content failed!",fielname.c_str());
            ifs.close();
            return false;
        }

        //关闭文件
        ifs.close();
        return true;
    }
};