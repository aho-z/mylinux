#ifndef __M_MYSQLUTIL_H__
#define __M_MYSQLUTIL_H__
#include "logger.hpp"
#include <mysql/mysql.h>
#include <string>
class mysql_util
{
public:
    //建立连接
    static MYSQL *mysql_create(const std::string &host,
                               const std::string &username,
                               const std::string &password,
                               const std::string &dbname,
                               uint16_t port = 3306)
    {
        // 1.初始化mysql句柄
        // MYSQL *mysql_init(MYSQL *mysql)
        // mysql为空则动态申请句柄空间进⾏初始化
        MYSQL *mysql = mysql_init(NULL);
        if (mysql == NULL)
        {
            ELOG("mysql init failed!");
            return NULL;
        }
        // 2.连接服务器
        if (mysql_real_connect(mysql, host.c_str(), username.c_str(), password.c_str(), dbname.c_str(), port, NULL, 0) == NULL)
        {
            ELOG("connect mysql server failed:%s", mysql_error(mysql));
            mysql_close(mysql); // 释放句柄
            return NULL;
        }

        // 3.设置客户端字符集
        if (mysql_set_character_set(mysql, "utf8") != 0)
        {
            ELOG("set client character failed:%s", mysql_error(mysql));
            mysql_close(mysql);
            return NULL;
        }

        return mysql;
    }

    //执行sql语句
    static bool mysql_exec(MYSQL *mysql, const std::string &sql)
    {
        int ret=mysql_query(mysql,sql.c_str());
        if(ret!=0){
            ELOG("%s\n",sql.c_str());
            ELOG("msyql query failed:%s\n",mysql_error(mysql));
            mysql_close(mysql);
            return false;
        }

        return true;
    }


    //关闭连接
    static void mysql_destroy(MYSQL *mysql)
    {
        if(mysql!=NULL)
        {
            mysql_close(mysql);
        }
        return;
    }
};

#endif