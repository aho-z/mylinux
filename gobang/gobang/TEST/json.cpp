#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <jsoncpp/json/json.h>



//使用jsoncpp库对多个数据对象进行序列化
std::string serialize()
{
    //1.将需要进行序列化的数据存储在Json::Value对象中
    Json::Value root;
    root["姓名"]="张三";
    root["年龄"]=18;
    root["成绩"].append(100);
    root["成绩"].append(60);
    root["成绩"].append(50);
    //2.实例化一个StreamWriterBuilder工厂类对象
    Json::StreamWriterBuilder swb;
    //3.通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
    Json::StreamWriter* sw=swb.newStreamWriter();
    //4.使用StreamWriter对象，对Json::value中存储的数据进行序列化
    std::stringstream ss;
    int ret=sw->write(root,&ss);
    if(ret!=0)
    {
        std::cout<<"json serialize failed!\n"<<std::endl;
        return "";
    }
    std::cout<<ss.str()<<std::endl;
    delete sw;
    return ss.str();
}

void unserialize(const std::string& str)
{
    //1.实例化一个charReaderBuilder工厂类对象
    Json::CharReaderBuilder crd;
    //2.使用工厂类对象生产一个CharReader对象
    Json::CharReader * cr=crd.newCharReader();
    //3.定义一个JSon::value对象接受存储解析的数据
    Json::Value root;
    std::string err;
    //4.使用charReader对象进行json格式字符串str的反序列化
    bool ret=cr->parse(str.c_str(),str.c_str()+str.size(),&root,&err); 
    if(ret==false) 
    {
        std::cout<<"json unserialize failed!\n"<<std::endl;
        return ;
    }
    //逐个提取json::value中的数据
    std::cout<<"姓名："<<root["姓名"].asCString()<<std::endl;
    std::cout<<"年龄："<<root["年龄"].asInt()<<std::endl;
    int sz=root["成绩"].size();
    for(int i=0;i<sz;++i)
    {
        std::cout<<"成绩： "<<root["成绩"][i].asFloat()<<std::endl;
    }

    // Json::Value root;
    // Json::Reader reder;
    // reder.parse(str,root);
    // std::cout<<"姓名："<<root["姓名"].asCString()<<std::endl;
    // std::cout<<"年龄："<<root["年龄"].asInt()<<std::endl;
    // int sz=root["成绩"].size();
    // for(int i=0;i<sz;++i)
    // {
    //     std::cout<<"成绩： "<<root["成绩"][i].asFloat()<<std::endl;
    // }

    delete cr;
}


int main()
{
    std::string str=serialize();
    unserialize(str);
    return 0;
}