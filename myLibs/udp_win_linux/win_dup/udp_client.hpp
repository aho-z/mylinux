#pragma once

#include<winSock2.h>
#include<stdio.h>
#include <string>
#include<cstring>
#include<iostream>
#include <thread>
namespace Client
{
    using namespace std;
    enum
    {
        USAGE_ERR = 1,
        SOCKET_ERR,
        BIND_ERR
    }; // 退出码

    class udpClient
    {
    public:
        udpClient(const string& serverip, const uint16_t& serverport)
            : serverip_(serverip), serverport_(serverport), sockfd_(-1), quit_(false)
        {
           

        }
        void initClient()
        {
            //****1.创建UDP网络通信端口****
            sockfd_ = socket(AF_INET, SOCK_DGRAM, 0);
            if (sockfd_ == -1)
            {
                cerr << "socket error: " << errno << " : " << strerror(errno) << endl;
                exit(SOCKET_ERR);
            }
            cout << "socket success: "
                << " : " << sockfd_ << endl;

            
        }

        static void* readMessage(void* args)
        {
            /*获取处理后的数据*/
            int sockfd_ = *(static_cast<int*>(args));
            
            
            while (true)
            {
                char buffer[1024];
                struct sockaddr_in temp; // 源Ip+源port
                int templen = sizeof(temp);
                size_t n = recvfrom(sockfd_, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&temp, &templen);
                if (n > 0)
                    buffer[n] = 0;
                
                cout << buffer << endl;
            }
            return nullptr;
        }

        void run()//主线程不断发消息，新线程不断收消息并打印
        {
            //reader_ = thread(readMessage, (void*)&sockfd_);
                /*********发送数据***********/
            struct sockaddr_in server; // 目的Ip+port
            memset(&server, 0, sizeof(server));
            server.sin_family = AF_INET;
            // 主机序列转网络序列
            server.sin_addr.s_addr = inet_addr(serverip_.c_str());
            server.sin_port = htons(serverport_);

            string message;

            while (!quit_)
            {
                fprintf(stderr, "Enter# ");
                fflush(stderr);
                getline(cin, message);
                message[message.size()] = 0;
                sendto(sockfd_, message.c_str(), message.size(), 0, (struct sockaddr*)&server, sizeof(server));

                char buffer[1024];
                struct sockaddr_in temp; // 源Ip+源port
                int templen = sizeof(temp);
                size_t n = recvfrom(sockfd_, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&temp, &templen);
                if (n > 0)
                    buffer[n] = 0;

                cout << buffer << endl;
            }
            //reader_.join();
            
                
            
        }

        ~udpClient()
        {
        }

    private:
        SOCKET sockfd_;
        string serverip_;
        uint16_t serverport_;
        bool quit_;

        thread reader_;
    };
}
