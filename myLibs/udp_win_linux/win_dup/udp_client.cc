//#pragma warning(disable:4996)
//#include<winSock2.h>
//#include<stdio.h>
//#include <string>
//#include<cstring>
//#include<iostream>
//
//#pragma comment(lib,"ws2_32.lib")
//using namespace std;
//
//uint16_t serverport = 8080;
//string serverip = "43.138.25.221";
//
//int main()
//{
//	WSAData wsd;
//	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
//	{
//		printf("Failed to load Winsock.\n"); //Winsock 初始化错误
//		return -1;
//	}
//	else
//	{
//		cout << "WSAStartup Success" << endl;
//	}
//	SOCKET csock = socket(AF_INET,SOCK_DGRAM,0);
//
//	if (csock==SOCKET_ERROR)
//	{
//		cout << "sock error=" << WSAGetLastError() << endl;
//		return 1;
//	}
//	else
//	{
//		cout << "socket Success"<<csock << endl;
//	}
//
//	struct sockaddr_in server;
//	memset(&server,0,sizeof(server));
//	server.sin_family = AF_INET;
//	server.sin_port = htons(serverport);
//	server.sin_addr.s_addr = inet_addr(serverip.c_str());
//
//
//	string line;
//	while (true)
//	{
//		//发送逻辑
//		cout << "Please Enter# ";
//		getline(cin,line);
//		
//		int n = sendto(csock,line.c_str(),line.size(),0,(struct sockaddr*)&server,sizeof(server));
//		if (n < 0)
//		{
//			cerr << "sendto error!" << endl;
//			break;
//		}
//		/*else
//		{
//			cout << "sendto success---"<<n << endl;
//		}*/
//		//收数据
//#define NUM 1024
//		char inbuffer[NUM];
//		struct sockaddr_in peer;
//		int peerlen = sizeof(peer);
//		inbuffer[0] = 0;
//		
//		n = recvfrom(csock,inbuffer,sizeof(inbuffer)-1,0, (struct sockaddr*)&peer,&peerlen);
//		
//		if (n > 0)
//		{ 
//			inbuffer[n] = 0;
//			cout << "server 返回的消息# " << inbuffer << endl;
//		}
//		else
//			break;
//	}
//	closesocket(csock);
//	WSACleanup();
//}
//
//
//
//



#pragma warning(disable:4996)
#include "udp_client.hpp"
#include <memory>
using namespace std;
using namespace Client;
#pragma comment(lib,"ws2_32.lib")

//客户端必须要知道发给谁
string serverip = "43.138.25.221";
uint16_t serverport = 8080;

static void Usage(string proc)
{
	cout << "\nUsage:\n\t" << proc << " server_ip server_port\n\n";
}

//udpClient server_ip server_port
int main()
{
	WSAData wsd;
	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
	{
		printf("Failed to load Winsock.\n"); //Winsock 初始化错误
		return -1;
	}
	else
	{
		cout << "WSAStartup Success" << endl;
	}
	


	unique_ptr<udpClient> ucli(new udpClient(serverip, serverport));
	ucli->initClient();
	ucli->run();

	WSACleanup();

	//return 0;
}