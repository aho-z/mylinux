#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <string>
#include <unistd.h>
#include "Protocol.hpp"
namespace Client
{
    enum
    {
        USAGE_ERR = 1,
        SOCKET_ERR,
        BIND_ERR,
        LISTEN_ERR
    };
    using namespace std;
    class tcpClient
    {
    public:
        tcpClient(const string &clientip, const uint16_t &clientport)
            : clientip_(clientip), clientport_(clientport), sockfd_(-1)
        {
        }
        void InitCient()
        {
            // 1.创建socket
            sockfd_ = socket(AF_INET, SOCK_STREAM, 0);
            if (sockfd_ < 0)
            {
                std::cerr << "socket create error" << endl;
                exit(2);
            }
        }
        void run()
        {
            struct sockaddr_in server;
            memset(&server, 0, sizeof(server));
            server.sin_family = AF_INET;
            server.sin_port = htons(clientport_);
            server.sin_addr.s_addr = inet_addr(clientip_.c_str());

            // 发起链接
            if (connect(sockfd_, (struct sockaddr *)&server, sizeof(server)) != 0)
            {
                std::cerr << "connect create error" << endl;
            }
            else
            {
                string msg;
                string inbuffer;
                while (true)
                {
                    cout << "mycal>>> ";
                    std::getline(std::cin, msg);
                    Request req = ParseLine(msg); // 从键盘提取字符串

                    string content;
                    // 序列化结构数据
                    req.serialize(&content);
                    // 添加报头
                    string send_string = enlength(content);
                    // 发送数据
                    send(sockfd_, send_string.c_str(), send_string.size(), 0);
                    

                    // 接收响应报文
                    
                    string package, text;
                    if (!recvRequest(sockfd_,inbuffer,&package))
                        continue;
                    
                    //去掉报头，获取正文放在text里面   
                    if(!delength(package,&text)) continue;
                    

                    //将收到的响应正文反序列化
                    Response resp;
                    resp.deserialize(text);
                    std::cout << "exitCode: " << resp.exitcode_ << std::endl;
                    std::cout << "result: " << resp.result_ << std::endl;     
                }
            }
        }
        Request ParseLine(const std::string &msg)
        {
            //"123+123"
            int status = 0; // 0:操作符之前 1:操作符 2:操作符之后
            int i = 0;
            int cnt = msg.size();
            std::string left, right;
            char op;

            while (i < cnt)
            {
                switch (status)
                {
                case 0:
                {
                    while (isdigit(msg[i]) && i < cnt)
                    {
                        left.push_back(msg[i++]);
                    }
                    op = msg[i];
                    status = 1;
                }
                break;

                case 1:
                    i++;
                    status = 2;
                    break;
                case 2:
                    right.push_back(msg[i++]);
                    break;
                }
            }
            std::cout << std::stoi(left) << " " << op << " " << std::stoi(right) << endl;
            return Request(std::stoi(left), std::stoi(right), op);
        }
        ~tcpClient()
        {
            if (sockfd_ >= 0)
                close(sockfd_);
        }

    private:
        int sockfd_;
        uint16_t clientport_;
        string clientip_;
    };
}