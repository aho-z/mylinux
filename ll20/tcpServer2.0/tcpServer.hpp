#pragma once

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <functional>
#include "logMessage.hpp"
#include "Protocol.hpp"
namespace Server
{
    enum
    {
        USAGE_ERR = 1,
        SOCKET_ERR,
        BIND_ERR,
        LISTEN_ERR
    };

    using namespace std;

    static const uint16_t gport = 8080;
    static const uint16_t gbacklog = 5;

    // req输入型参数，resp输出型参数
    typedef function<bool(const Request &req, Response &rep)> func_t;

    

    void handlerEnter(int sock, func_t func)
    {
        string inuffer;//将所有信息写入到inbuffer
        while(true)
        {
            // 1.读取："content_len"\r\n"x op y"\r\n
            // 1.1 保证读到的消息是【一个完整】的请求
            std::string req_text; // 输出型参数，整个报文
            if (!recvRequest(sock, inuffer,&req_text))
                return;
            std::cout<<"带报头的请求：\n"<<req_text<<endl;    
            // 1.2 去报头，只要正文
            std::string req_str; // 正文部分
            if (!delength(req_text, &req_str))
                return;
            std::cout<<"去掉报头后的正文：\n"<<req_str<<endl; 

            // 2.反序列化
            // 2.1 得到一个结构化对象，对象中的成员已经被填充
            Request req;
            if (!req.deserialize(req_str))
                return;

            // 3.处理数据---------业务逻辑
            // 3.1 得到一个结构化的响应,resp成员已被填充
            Response resp;
            func(req, resp); // 回调

            // 4.对响应Response,序列化
            // 4.1 得到一个字符串
            std::string resp_str;
            resp.serialize(&resp_str); // 输出型参数，将序列化结果写入resp_str
            std::cout<<"计算完成，序列化响应: "<<resp_str<<endl;

            // 5.然后发送响应
            // 5.1添加协议报头，构建成一个完整的报文
            std::string send_string = enlength(resp_str);
            std::cout<<"构建带报头的响应正文: \n"<<send_string<<endl;

            // 发送
            send(sock, send_string.c_str(), send_string.size(), 0); // 有问题
            std::cout<<"发送响应报文成功: \n"<<endl;
        }
    }

    class tcpServer
    {
    public:
        tcpServer(const uint16_t &port = gport)
            : listen_sockfd_(-1), port_(port)
        {
        }

        void InitServer()
        {
            // 1.创建socket
            listen_sockfd_ = socket(AF_INET, SOCK_STREAM, 0);
            if (listen_sockfd_ < 0)
            {
                logMessage(FATAL, "create socket error");
                exit(SOCKET_ERR);
            }
            logMessage(NORMAL, "create socket success");

            // 2.bind网络信息
            struct sockaddr_in local;
            memset(&local, 0, sizeof(local));
            local.sin_family = AF_INET;
            local.sin_port = htons(port_);
            local.sin_addr.s_addr = INADDR_ANY;
            if (bind(listen_sockfd_, (struct sockaddr *)&local, sizeof(local)) < 0)
            {
                logMessage(FATAL, "bind socket error");
                exit(BIND_ERR);
            }

            logMessage(NORMAL, "bind socket success:%d", listen_sockfd_);

            // 3.设置socket为监听状态
            if (listen(listen_sockfd_, gbacklog) < 0)
            {
                logMessage(FATAL, "listen socket error");
                exit(LISTEN_ERR);
            }
            logMessage(NORMAL, "listen socket success");
        }

        void start(func_t func)
        {

            for (;;)
            {
                // 4.server获取新链接
                struct sockaddr_in peer;
                socklen_t len = sizeof(peer);
                // sock:和客户端通信的文件描述符
                int sock = accept(listen_sockfd_, (struct sockaddr *)&peer, &len);
                if (sock < 0) // 没有获取新链接成功就执行下一次循环
                {
                    logMessage(FATAL, "accpect sock error");
                    continue;
                }
                logMessage(NORMAL, "accept sock success,get new sock:%d", sock);

                pid_t id = fork();
                if (id == 0) // 子进程
                {
                    close(listen_sockfd_);
                    // if (fork() > 0)
                    //     exit(0); // 也可以用信号

                    handlerEnter(sock, func);
                    close(sock);
                    exit(0);
                }

                close(sock);
                // 父进程
                pid_t ret = waitpid(id, nullptr, 0);
                if (ret > 0)
                {
                    logMessage(NORMAL, "waitpid success");
                }
            }
        }

        ~tcpServer()
        {
        }

    private:
        int listen_sockfd_; // 不负责通信，只负责监听链接，获取新链接
        uint16_t port_;
    };
}