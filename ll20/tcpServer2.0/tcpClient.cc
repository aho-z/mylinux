#include "tcpClient.hpp"
#include <memory>
using namespace std;
using namespace Client;
static void Usage(string proc)
{
    cout << "\nUsage:\n\t" << proc << " server_ip server_port\n\n";
}


int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        Usage(argv[0]);
        exit(USAGE_ERR);
    }
    uint16_t clientport=atoi(argv[2]);
    string clientip=argv[1];

    unique_ptr<tcpClient> ucli(new tcpClient(clientip,clientport));
    ucli->InitCient();
    ucli->run();
    return 0;
}