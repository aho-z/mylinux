#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>

int main(){
  int x=000;
  pid_t ret=fork();
  if(ret==0){
      //子进程
    while(1){
        printf("这是子进程，它的pid为：%d,它的父进程pid为：%d,%d,%p\n",getpid(),getppid(),x,&x);
        sleep(1);
    }
  }
  else if (ret>0){
      //父进程
    while(1){
      
        printf("这是父进程，它的pid为：%d,它的父进程pid为：%d,%d,%p\n",getpid(),getppid(),x,&x);
        x=9999;
        sleep(1);
    }
  }
  else{}

  return 0;
}
