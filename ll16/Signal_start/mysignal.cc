#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <cstdlib>
#include <string>
#include <cstdio>
using namespace std;

int main()
{
    //核心转储
    while(true)
    {
        int a[10];
        //a[100]=10;//没报错
        a[10000] = 10;
    }
}


// void catchSig(int signo)
// {
//     std::cout<<"获取到一个信号，信号编号是:"<<signo<<std::endl;
//     exit(1);
// }
// int main()
// {
//     signal(SIGALRM,catchSig);
//     alarm(1);
//     int cnt=0;
//     while (true)
//     {
//         std::cout<<"cnt: "<<cnt++<<std::endl;
//     }
    
// }






// void UseBook(const std::string& proc)
// {
//      std::cout << "\nUseBook: " << proc << " pid signo\n"
//               << std::endl;
// }


// void catchSig(int signo)
// {
//     std::cout<<"获取到一个信号，信号编号是:"<<signo<<std::endl;
//     exit(1);
// }
// int main(int argc,char*argv[])
// {
//     //收到11号信号才会调用catchSig
//     //signal(11,catchSig);
//     while(true)
//     {
//         std::cout<<"我在运行中..."<<std::endl;
//         sleep(1);
//         int*p = nullptr;
//         *p=10;
//     }
// }





// void catchSig(int signo)
// {
//     cout<<"获取到一个信号，信号编号是:"<<signo<<endl;
// }
// int main(int argc,char*argv[])
// {
   
//     signal(SIGFPE,catchSig);//捕捉任意信号
//     int a = 1;
//     a/=0;//只执行一次，却一直打印
//     while(true)
//     {
//         cout<<"我在运行中..."<<endl;
//         sleep(1);
//     }
// }
// int main()
// {
//     while (true)
//     {
//         std::cout << "我是一个进程: " << getpid() << std::endl;
//         sleep(1);
//         int a=1;
//         a/=0;//除0错误
//     }
    
// }

// int main()
// {
//     //abort给自己发送6号信号【SIGABRT】
//     int cnt=0;
//     while(cnt<=10)
//     {
//         std::cout<<"cnt: "<<cnt++<<std::endl;
//         sleep(1);
//         if(cnt>=5) abort();
//     }
// }


// int main()
// {
//     //int raise(int sig);  给自己发送任意信号
//     //这里原本是打印十次cnt，
//     //修改后：当cnt>=5的时候，让进程退出
//     int cnt=0;
//     while(cnt<=10)
//     {
//         std::cout<<"cnt: "<<cnt++<<std::endl;
//         sleep(1);
//         if(cnt>=5) raise(3);
//     }
// }


/*****************************************/
//argc：命令行参数个数
//argv：命令行参数字符串
// int main(int argc,char*argv[])
// {
//     //例如 kill       36892       2
//     //    argv[0]     argv[1]     argv[2]
//     if(argc!=3)//kill可以向任何进程发送任何信号
//     {
//         UseBook(argv[0]);
//         exit(1);
//     }
//     pid_t pid=atoi(argv[1]);
//     int signo=atoi(argv[2]);
    
//     int n=kill(pid,signo);
//     if(n!=0) perror("kill");

// }





/***********硬件发送产生*************/
// void handler(int signo)
// {
//     std::cout << "进程捕捉到了一个信号，信号编号是: " << signo << std::endl;
//     // exit(0);
// }

// int main()
// {
//     // 这里是signal函数的调用，并不是handler的调用
//     /// 仅仅是设置了对2号信号的捕捉方法，并不代表该方法被调用了
//     // 一般这个方法不会执行，除非收到对应的信号！
//     signal(2, handler);

//     while(true)
//     {
//         std::cout << "我是一个进程: " << getpid() << std::endl;
//         sleep(1);
//     }
// }