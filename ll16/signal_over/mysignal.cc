#include <iostream>
#include <signal.h>
#include <unistd.h>
using namespace std;

void Count(int cnt)
{
    while(cnt)
    {
        printf("cnt:%d\r",cnt);
        fflush(stdout);
        cnt--;
        sleep(1);
    }
    cout<<endl;
}

void handler(int signo)
{
    cout<<"get a signo"<<signo<<"正在处理信号"<<endl;
    Count(20);//倒计时打印
}

int main()
{
    struct sigaction act,oact;
    act.sa_handler=handler;
    act.sa_flags=0;
    sigemptyset(&act.sa_mask);
    //屏蔽多个信号可以把信号添加到sa_mask
    sigaddset(&act.sa_mask,3);
    

    sigaction(SIGINT,&act,&oact);

    while (true)
    {
        sleep(1);
    }
    return 0;
}