#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

void handler(int signo)
{

    // 1. 多个子进程，在同一个时刻退出了
    // 2. 多个子进程，在同一个时刻只有一部分退出了
    // while(1)//等待所有要退出的子进程
    // {
    //     pid_t ret = waitpid(-1, NULL, WNOHANG);//非阻塞式调用
    //     if(ret == 0) break;
    // }

    // printf("pid: %d, %d 号信号，正在被捕捉!\n", getpid(), signo);
    // printf("quit: %d", quit);
    // quit = 1;
    // printf("-> %d\n", quit);
}

// void Count(int cnt)
// {
//     while (cnt)
//     {
//         printf("cnt: %2d\r", cnt);
//         fflush(stdout);
//         cnt--;
//         sleep(1);
//     }
//     printf("\n");
// }

int main()
{
    // 显示的设置对SIGCHLD进行忽略
    signal(SIGCHLD, SIG_IGN);//忽略
    signal(SIGCHLD, SIG_DFL);//默认

    printf("我是父进程, %d, ppid: %d\n", getpid(), getppid());

    pid_t id = fork();
    if (id == 0)
    {
        printf("我是子进程, %d, ppid: %d，我要退出啦\n", getpid(), getppid());
        Count(5);
        exit(1);
    }

    while (1)
        sleep(1);

    // signal(2, handler);
    // while(!quit);
    // printf("注意， 我是正常退出的!\n");
    return 0;
}




// int quit = 0;
// void handler(int signo)
// {
//     printf("%d号信号，正在被捕捉\n",signo);
//     printf("quit:%d",quit);
//     quit = 1;
//     printf("->%d\n",quit);
// }
// int main()
// {
//     signal(2,handler);
//     while(!quit);
//     printf("注意，我是正常退出的\n");
//     return 0;
// }

